/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/24 16:15:37 by mivanov           #+#    #+#             */
/*   Updated: 2016/11/26 21:03:27 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncpy(char *dst, const char *src, size_t len)
{
	size_t	i;
	size_t	t;

	i = 0;
	t = len;
	while (len-- > 0)
	{
		dst[i] = src[i];
		i++;
	}
	i = 0;
	while (src[i])
		i++;
	while (i < t)
		dst[i++] = 0;
	return (dst);
}
