/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 11:45:54 by mivanov           #+#    #+#             */
/*   Updated: 2016/12/02 15:07:21 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	void	*area;
	size_t	i;

	i = 0;
	area = (void*)malloc(size * sizeof(*area));
	if (!area)
		return (NULL);
	while (i < size)
	{
		((char*)area)[i] = 0;
		i++;
	}
	return (area);
}
