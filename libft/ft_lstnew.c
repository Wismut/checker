/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 19:17:46 by mivanov           #+#    #+#             */
/*   Updated: 2016/12/01 18:09:05 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	void	*data;
	t_list	*newl;

	newl = (t_list*)malloc(sizeof(t_list));
	if (!content)
	{
		newl->content = NULL;
		return (newl);
	}
	data = (void*)content;
	if (!newl)
		return (NULL);
	if (!(newl->content = malloc(content_size)))
		return (NULL);
	newl->content_size = content_size;
	ft_memmove(newl->content, content, content_size);
	return (newl);
}
