/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 18:13:08 by mivanov           #+#    #+#             */
/*   Updated: 2016/12/01 18:09:31 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	size_t	m;

	m = n;
	while (n-- > 0)
		*(unsigned char *)dst++ = *(unsigned char *)src++;
	while (m-- > 0)
		dst--;
	return (dst);
}
