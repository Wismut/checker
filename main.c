#include "checker.h"

int		is_flag(char *str)
{
	if (!ft_strcmp(str, "-v") || !ft_strcmp(str, "-c") ||
			!ft_strcmp(str, "-l") || !ft_strcmp(str, "-n") ||
			!ft_strcmp(str, "-f") || !ft_strcmp(str, "-m"))
		return (1);
	return (0);
}

void	alloc_mem(t_stack **stack_a, t_stack **head_a, long value)
{
	if (!*head_a)
	{
		*stack_a = (t_stack *)malloc(sizeof(t_stack));
		(*stack_a)->value = value;
		(*stack_a)->next = NULL;
		*head_a = *stack_a;
	}
	else
	{
		(*head_a)->next = (t_stack *)malloc(sizeof(t_stack));
		*head_a = (*head_a)->next;
		(*head_a)->value = value;
		(*head_a)->next = NULL;
	}
}

void	create_elems(int argc, t_flags **flags, t_stack **stack_a, char **argv)
{
	t_stack	*head_a;
	int		count;
	long	value;

	count = 0;
	head_a = *stack_a;
	while (++count < argc)
	{
		if (is_flag(argv[count]))
			add_flag(flags, argv[count]);
		else
		{
			is_valid(argv[count]);
			value = ft_atol(argv[count]);
			value < -2147483648 || value > 2147483647 ? ft_error("Error\n") : 0;
			cheak_repeat(*stack_a, value);
			alloc_mem(stack_a, &head_a, value);
		}
	}
}

int		main(int argc, char **argv)
{
	t_stack	*a;
	t_stack	*b;
	t_flags	*flag;
	char	*operation;

	flag = create_new_flags();
	b = NULL;
	if (argc == 1)
		ft_error("Please insert values between -2147483648 and 2147483647\n");
	create_elems(argc, &flag, &a, argv);
	flag->f && flag->c ? print_stack(a, b, "\x1b[34m", "\x1b[33m") : 0;
	flag->f && !flag->c ? print_stack(a, b, "\x1b[0m", "\x1b[0m") : 0;
	flag->m ? print_count_in_stack(a, b) : 0;
	while (get_next_line(0, &operation))
	{
		make_instructions(&a, &b, operation);
		free(operation);
		flag->v && !flag->c ? print_stack(a, b, "\x1b[0m", "\x1b[0m") : 0;
		flag->v && flag->c ? print_stack(a, b, "\x1b[34m", "\x1b[33m") : 0;
		flag->n ? print_count_in_stack(a, b) : 0;
	}
	flag->f && flag->c ? print_stack(a, b, "\x1b[34m", "\x1b[33m") : 0;
	flag->f && !flag->c ? print_stack(a, b, "\x1b[0m", "\x1b[0m") : 0;
	flag->m ? print_count_in_stack(a, b) : 0;
	print_answer(a, b);
}
