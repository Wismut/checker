#ifndef CHECKER_H
# define CHECKER_H

# include <string.h>
# include <unistd.h>
# include <stdlib.h>
# include "libft/libft.h"

typedef struct		s_stack
{
	int				value;
	struct s_stack	*next;
}					t_stack;

typedef struct		s_flags
{
	int				c;
	int				v;
	int				l;
	int				n;
	int				f;
	int				m;
}					t_flags;

void				reverse_rotate_a(t_stack **stack_a);
void				reverse_rotate_b(t_stack **stack_b);
void				reverse_rotate_a_b(t_stack **stack_a, t_stack **stack_b);
void				rotate_a(t_stack **stack_a);
void				rotate_b(t_stack **stack_b);
void				rotate_a_b(t_stack **stack_a, t_stack **stack_b);
void				push_a(t_stack **stack_a, t_stack **stack_b);
void				push_b(t_stack **stack_a, t_stack **stack_b);
void				swap_a(t_stack **stack_a);
void				swap_b(t_stack **stack_b);
void				swap_a_b(t_stack **stack_a, t_stack **stack_b);
int					count_elements(t_stack *stack);
void				ft_error(char *error);
void				make_instructions(t_stack **stack_a, t_stack **stack_b,
char *instr);
void				cheak_repeat(t_stack *stack, int number);
void				print_answer(t_stack *stack_a, t_stack *stack_b);
void				print_stack(t_stack *stack_a, t_stack *stack_b,
								char *color1, char *color2);
int					number_size(int n);
t_flags				*create_new_flags(void);
void				add_flag(t_flags **flag, char *str);
void				print_count_in_stack(t_stack *stack_a, t_stack *stack_b);
void				is_valid(char *string);
void				ft_error(char *error);
long				ft_atol(char *str);

#endif
