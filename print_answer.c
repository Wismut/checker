#include "checker.h"

int		is_stack_sorted(t_stack *stack)
{
	if (!stack)
		return (0);
	if (!stack->next)
		return (1);
	while (stack->next)
	{
		if (stack->value > stack->next->value)
			return (0);
		stack = stack->next;
	}
	return (1);
}

int		is_stack_empty(t_stack *stack)
{
	if (!stack)
		return (1);
	return (0);
}

void	print_answer(t_stack *stack_a, t_stack *stack_b)
{
	if (is_stack_sorted(stack_a) && is_stack_empty(stack_b))
		ft_putstr("OK\n");
	else
		ft_putstr("KO\n");
}
