#include "checker.h"

int		get_count_to_print(t_stack *stack)
{
	return (12 - (stack ? number_size(stack->value) : 0));
}

void	print_stack(t_stack *stack_a, t_stack *stack_b, char *color1,
					char *color2)
{
	ft_putstr(color1);
	ft_putstr("       A       |");
	ft_putstr(color2);
	ft_putstr("|       B      \n");
	ft_putstr(color1);
	ft_putstr("---------------|");
	ft_putstr(color2);
	ft_putstr("|---------------\n");
	while (stack_a || stack_b)
	{
		ft_putstr(" ");
		ft_putstr(color1);
		stack_a ? ft_putnbr(stack_a->value) : 0;
		write(1, "            ", get_count_to_print(stack_a));
		stack_a ? stack_a = stack_a->next : 0;
		ft_putstr("  |");
		ft_putstr(color2);
		ft_putstr("|  ");
		stack_b ? ft_putnbr(stack_b->value) : 0;
		write(1, "            ", get_count_to_print(stack_b));
		stack_b ? stack_b = stack_b->next : 0;
		ft_putstr(" \n");
	}
	ft_putstr("\x1b[0m");
}
