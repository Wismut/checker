#include "checker.h"

int		number_size(int n)
{
	int	result;

	result = 0;
	if (n <= 0)
		result++;
	while (n != 0)
	{
		n /= 10;
		result++;
	}
	return (result);
}

int		count_elements(t_stack *stack)
{
	int	i;

	i = 0;
	while (stack)
	{
		stack = stack->next;
		i++;
	}
	return (i);
}
