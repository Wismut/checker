#include "checker.h"

void	make_instructions(t_stack **stack_a, t_stack **stack_b, char *instr)
{
	if (!ft_strcmp(instr, "pa"))
		push_a(stack_a, stack_b);
	else if (!ft_strcmp(instr, "pb"))
		push_b(stack_a, stack_b);
	else if (!ft_strcmp(instr, "ra"))
		rotate_a(stack_a);
	else if (!ft_strcmp(instr, "rb"))
		rotate_b(stack_b);
	else if (!ft_strcmp(instr, "sa"))
		swap_a(stack_a);
	else if (!ft_strcmp(instr, "sb"))
		swap_b(stack_b);
	else if (!ft_strcmp(instr, "ss"))
		swap_a_b(stack_a, stack_b);
	else if (!ft_strcmp(instr, "rr"))
		rotate_a_b(stack_a, stack_b);
	else if (!ft_strcmp(instr, "rrr"))
		reverse_rotate_a_b(stack_a, stack_b);
	else if (!ft_strcmp(instr, "rra"))
		reverse_rotate_a(stack_a);
	else if (!ft_strcmp(instr, "rrb"))
		reverse_rotate_b(stack_b);
	else
		ft_error("Error\n");
}
