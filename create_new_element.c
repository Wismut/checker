#include "checker.h"

t_flags		*create_new_flags(void)
{
	t_flags	*flags;

	flags = (t_flags *)malloc(sizeof(t_flags));
	flags->c = 0;
	flags->l = 0;
	flags->v = 0;
	flags->n = 0;
	flags->f = 0;
	flags->m = 0;
	return (flags);
}
