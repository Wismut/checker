#include "checker.h"

long	ft_atol(char *str)
{
	unsigned long	res;
	int				i;
	int				isneg;

	res = 0;
	i = 0;
	while ((str[i] >= 9 && str[i] <= 13) || str[i] == 32)
		i++;
	if ((str[i] == '-' || str[i] == '+')
		&& (str[i + 1] > '9' || str[i + 1] < '0'))
		return (0);
	isneg = str[i] == '-' ? -1 : 0;
	str[i] == '-' || str[i] == '+' ? i++ : 0;
	while (str[i])
	{
		if (str[i] < '0' || str[i] > '9')
			break ;
		res = res * 10 + (str[i] - '0');
		i++;
	}
	return (isneg < 0 ? -res : res);
}

void	ft_error(char *error)
{
	ft_putstr(error);
	exit(0);
}

void	is_valid(char *string)
{
	int	i;

	i = 0;
	if (!string)
		ft_error("Error\n");
	if (ft_strlen(string) > 11)
		ft_error("Error\n");
	while (string[i])
	{
		if (((string[i] == '+' || string[i] == '-') && i != 0) ||
(string[i] < '0' && string[i] != '-' && string[i] != '+') || string[i] > '9')
			ft_error("Error\n");
		i++;
	}
}

void	add_flag(t_flags **flag, char *str)
{
	if (!ft_strcmp(str, "-v"))
		(*flag)->v = 1;
	else if (!ft_strcmp(str, "-c"))
		(*flag)->c = 1;
	else if (!ft_strcmp(str, "-l"))
		(*flag)->l = 1;
	else if (!ft_strcmp(str, "-n"))
		(*flag)->n = 1;
	else if (!ft_strcmp(str, "-f"))
		(*flag)->f = 1;
	else if (!ft_strcmp(str, "-m"))
		(*flag)->m = 1;
}

void	print_count_in_stack(t_stack *stack_a, t_stack *stack_b)
{
	ft_putstr("A: ");
	ft_putnbr(count_elements(stack_a));
	ft_putstr("\nB: ");
	ft_putnbr(count_elements(stack_b));
	ft_putstr("\n");
}
