#include "checker.h"

void	reverse_rotate_a(t_stack **stack_a)
{
	t_stack	*head;
	t_stack	*last;
	t_stack	*before_last;

	if (count_elements(*stack_a) <= 1)
		return ;
	head = *stack_a;
	before_last = *stack_a;
	while (before_last->next->next)
		before_last = before_last->next;
	last = before_last->next;
	before_last->next = NULL;
	last->next = head;
	*stack_a = last;
}

void	reverse_rotate_b(t_stack **stack_b)
{
	t_stack	*head;
	t_stack	*last;
	t_stack	*before_last;

	if (count_elements(*stack_b) <= 1)
		return ;
	head = *stack_b;
	before_last = *stack_b;
	while (before_last->next->next)
		before_last = before_last->next;
	last = before_last->next;
	before_last->next = NULL;
	last->next = head;
	*stack_b = last;
}

void	reverse_rotate_a_b(t_stack **stack_a, t_stack **stack_b)
{
	reverse_rotate_a(stack_a);
	reverse_rotate_b(stack_b);
}
